package src.main.java.practice;

public class test2 {

    public static int getEvenDigitSum(int number){

        if(number<0){
            return-1;
        }
        int sum = 0;
        while(number>0){
            int newNumber=number%10;
            if(newNumber%2==0){

                sum+=newNumber;

            }
            number/=10;
        }
        return sum;
    }

    public static void main(String[] args) {
        System.out.println("even sum is" +(getEvenDigitSum(2000)));
    }
}

