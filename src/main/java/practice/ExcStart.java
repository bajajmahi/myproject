package test.src.main.java.practice;
/*
 * Extend ExcStart.java so that it builds a linked list out of the int elements in the “nums” array! Use the
 * LL class to construct the linked list. Use the “listHead” variable to store the head of the list.
 */

import java.util.LinkedList;

public class ExcStart {
    static final int nums[] = {32, 17, 8, 9};
    LL listHead;

    class LL {
        int num;
        int head;
        LL backlink;
        LinkedList<Integer> list;

        public void toList(int[] arr) {
            list = new LinkedList<Integer>();
            for (int i : arr)
                list.add(i);
        }
    }

    void createLL(int nums[]) {
        LL l = new LL();
        l.toList(nums);
    }

    int getHead(LinkedList<Integer> list) {
        return list.getFirst();
    }

    public static void main(String args[]) {
        ExcStart mylist = new ExcStart();
        // LL myl = mylist.createLL(nums);
        System.out.println();
        // System.out.println(mylist.createLL(nums));
    }

}


