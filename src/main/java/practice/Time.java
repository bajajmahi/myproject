package test.src.main.java.practice;

import java.util.Scanner;

public class Time {

    public static void main(String[] Strings) {


        double minutesInYear = 60 * 24 * 365;

        int day = 60 * 24;

        Scanner input = new Scanner(System.in);

        System.out.print("Input the number of minutes: ");

        double min = input.nextDouble();

        long years = (long) (min / minutesInYear);
        int days = (int) (min / day) % 365;

        System.out.println((int) min + " minutes is approximately " + years + " years and " + days + " days");
    }
}

