package src.main.java.practice;

public class LargestPrime {
    public static void main(String[] args) {
        System.out.println("Largest prime factor is " +getLargestPrime( 78 ));
    }
    public static int getLargestPrime(int number){
        if(number <=1)
        {
            return -1;

        }else{
            int copyOfNumber = number;

            for (int i = copyOfNumber-1; i > 1; i--) {
                if (number % i == 0) {
                    number = i;
                }
            } return number;
        }

    }
}