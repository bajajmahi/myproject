package src.main.java.practice;

import java.util.Scanner;

public class Main5 {
    private static Scanner scanner=new Scanner( System.in );
    private static GroceryList groceryList=new GroceryList();

    public static void main(String[] args) {
        boolean quit=false;
        int choice=0;
        printInstructions();
        while (!quit){
            System.out.println("Enter your Choice ");
            choice=scanner.nextInt();
            scanner.nextLine();
            switch (choice){

                case 0:
                    printInstructions();
                    break;
                case 1:
                    groceryList.printGroceryList();
                    break;
                case 2:
                    addItem();
                    break;
                case 3:
                    modifyItem();
                    break;
                case 4:
                    removeItem();
                    break;

                case 5:
                    searchForItem();
                    break;
                    default:
                        quit=true;
                        break;


            }
        }
    }
    public static  void printInstructions(){
        System.out.println("\n Press");
        System.out.println("\t 0-To print choice option");
        System.out.println("\t 1-To print grocerylist items");
        System.out.println("\t 2-To add an  item into grocery list");
        System.out.println("\t 3-To modify item in grocery list");
        System.out.println("\t 4-To remove item in grocery list");
        System.out.println("\t 5-To search item grocery list");
        System.out.println("\t 6-To quit  the Application");


    }
    public static void addItem(){
        System.out.println("Please Add your item");
        groceryList.addGroceryItem( scanner.nextLine() );
    }



    public static void modifyItem(){
        System.out.println(" Enter item number");
        int itemNumber=scanner.nextInt();
        scanner.nextLine();
        System.out.println("Enter relpaced item");
        String newItem=scanner.nextLine();
        groceryList.modifyGroceryList( itemNumber-1,newItem );

    }
    public static void removeItem(){

        System.out.println("Enter item number");
        int itemeNumber=scanner.nextInt();
        scanner.nextLine();
        groceryList.removeGroceryItem( itemeNumber-1 );

    }
    public static void searchForItem(){
        System.out.println("Enter item to search for");
        String itemSearch=scanner.nextLine();
        if(groceryList.findItem(itemSearch)!=null){
            System.out.println("Found " +itemSearch+ "in grocery list");

        }else {
            System.out.println("Item not found");
        }

        }


    }



