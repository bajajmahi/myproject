package src.main.java.practice.com.company;

public class EvenDigitSum {
    public static void main(String[] args) {
        System.out.println("Sum of Even Digit is: " + (getEvenDigitSum(2000)));
    }

    public static int getEvenDigitSum(int number) {

        if (number < 0) {
            return -1;
        }
        int sum = 0;
        int lastDig = 0;
        while (number !=0 ) {

            lastDig = number % 10;
            number = number / 10;
            //if(lastDig%2==0){

            if (lastDig % 2 != 0) {
                continue;
            }
//            if (lastDig > 10) {
//                sum += lastDig / 10;
//                sum += lastDig % 10;
            sum += lastDig;
            }


        return sum;
    }
}
