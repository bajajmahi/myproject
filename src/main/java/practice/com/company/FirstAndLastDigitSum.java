package src.main.java.practice.com.company;

public class FirstAndLastDigitSum {
    public static void main(String[] args) {
        System.out.println("The Sum of First and Last Digit of Given Number is :" + (sumFirstAndLastDigit(5)));

    }

    public static int sumFirstAndLastDigit(int number) {
        if (number < 0) {
            return -1;
        } else {

            int sum = 0;
            int lastDigit = number % 10;
            int firstDigit = number;
            while (number > 0) {
                firstDigit = number % 10;
                number /= 10;
                sum = firstDigit + lastDigit;

            }
            return sum;
        }
    }
}
