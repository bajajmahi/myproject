package src.main.java.practice.com.company;

public class SharedDigit {
    public static void main(String[] args) {
        System.out.println((hasSharedDigit(23,34)));

    }

    public static boolean hasSharedDigit(int num1, int num2) {

        if ((num1 < 10) || (num2 < 10) || (num1 > 99) || (num2 > 99)) {
            return false;
        }
        int lastDigitOne = num1 % 10;
        int lastDigitTwo = num2 % 10;
        int firstDigitOne = num1  / 10;
        int firstDigitTwo = num2  / 10;

        if ((lastDigitOne == lastDigitTwo) || (lastDigitTwo == firstDigitOne) || (firstDigitTwo == lastDigitOne) || (firstDigitOne == firstDigitTwo)) {
            return true;
        } else {
            return false;
        }
    }


}
