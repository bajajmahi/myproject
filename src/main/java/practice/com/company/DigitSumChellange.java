package src.main.java.practice.com.company;


public class DigitSumChellange {

    public static void main(String[] args) {
        System.out.println("The sum of Digit 125 is :" + (sumDigit(125)));
    }

    private static int sumDigit(int number) {

        if (number < 10) {
            return -1;
        }
        int sum = 0;
        while (number > 0) {

            int digit = number % 10;
            sum += digit;
            number /= 10;
        }
        return sum;
    }
}
