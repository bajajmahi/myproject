package src.main.java.practice.com.company;

public class PalindromNumber {
    public static void main(String[] args) {
        System.out.println("122 is not a Pallindrom number :" + (isPallindrome(122)));
    }

    public static boolean isPallindrome(int number) {
        int reverse = 0;
        int newNumber = number;
        while (newNumber != 0) {
            int lastDigit = newNumber % 10;
            reverse = reverse * 10;
            reverse += lastDigit;
            newNumber /= 10;
        }
        if (number == reverse) {
            return true;
        } else {
            return false;
        }
    }
}
