package src.main.java.practice;

import java.util.Scanner;

public class MinElement {
    private static Scanner scanner = new Scanner( System.in );

    public static void main(String[] args) {
        System.out.println( "Enter Count :" );
        int count = scanner.nextInt();
        scanner.nextLine();

        int[] returnedArray = readIntegers( count );

        int returnedMin=findMin( returnedArray );
        System.out.println("min =  "+returnedMin);

    }

    public static int[] readIntegers(int count) {
        int[] array = new int[ count ];
        System.out.println( "enter " + count + " integers values : \r" );
        for (int i = 0; i < array.length; i++) {
            array[ i ] = scanner.nextInt();
        }
        return array;

    }

//    public static void printArray(int [] array) {
//
//        for (int i = 0; i < array.length; i++) {
//            System.out.println( "Element:" + i + "contents" + array[ i ] );
//
//        }
//    }


    private static int findMin(int[] array) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < array.length; i++) {
            int value=array[i];
            if(value<min){
                min=value;
            }

        }
      return min;

    }
}