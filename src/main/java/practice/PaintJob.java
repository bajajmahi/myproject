package src.main.java.practice;

public class PaintJob {
    public static void main(String[] args) {
        System.out.println("Bucket Needed for work is : " + getBucketCount(3.23,1.5));
        System.out.println("Bucket Needed for work is : " +getBucketCount( 5.45,3.24,1.5,2 ));
    }
    public static int getBucketCount(double widht, double height,double areaPerBucket, int extraBucket){
        if(widht<=0||height<=0||areaPerBucket<=0||extraBucket<0){
            return-1;
        }else{
            int bucketCount= (int) Math.ceil( (((widht*height)/areaPerBucket)-extraBucket));
            return bucketCount;
        }


    }
    public static int getBucketCount(double widht ,double height,double areaPerBucket){

        if(widht<=0||height<=0||areaPerBucket<=0){
            return-1;
        } else{
            int bucketCount= (int)Math.ceil ((widht*height)/areaPerBucket);
            return bucketCount;
        }
    }
    public static int getBucketCount(double area,double areaPerBucket){

        if(area<=0||areaPerBucket<=0){
            return-1;
        }else{
            int bucketCount= (int) Math.ceil(area/areaPerBucket);
            return bucketCount;
        }
    }
}


