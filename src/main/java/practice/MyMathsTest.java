package src.main.java.practice;

import static org.testng.Assert.*;

public class MyMathsTest {

    @org.testng.annotations.Test
    public void testAdd2integers() {
        assertEquals( MyMaths.add2integers( 3,5 ), 8);
        assertEquals( MyMaths.add2integers( 3,3 ), 6);
        assertEquals( MyMaths.multiply( 3,3 ), 9);

    }
}