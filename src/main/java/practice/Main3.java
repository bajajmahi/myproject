package src.main.java.practice;

 class Car{
    private boolean engine;
    private int cylinder;
    private int wheels;
    private String name;

    public Car( int cylinder, String name) {
        this.engine = true;
        this.cylinder = cylinder;
        this.wheels = 4;
        this.name=name;
    }
    public int getCylinder() {
         return cylinder;
     }
     public String getName() {
         return name;
     }
     public String starEngine(){
        return"Car->startEngine";

     }
     public String accelerate(){
        return "car->accelerate";
     }
     public String  brake(){
        return "car->brake";
     }

     }
class Swift extends Car{

    public Swift(int cylinder, String name) {
        super( cylinder, name );
    }

    @Override
    public String starEngine() {
        return "Swift->startEngine";
    }

    @Override
    public String accelerate() {
        return "Swift->accelerate";
    }

    @Override
    public String brake() {
        return "Swift->brake";
    }
}
    class Hundai extends Car{
        public Hundai(int cylinder, String name) {
            super( cylinder, name );
        }

        @Override
        public String starEngine() {
            return "Hundai->startEngine";
        }

        @Override
        public String accelerate() {
            return "Hundai->accelerate";
        }

        @Override
        public String brake() {
            return "Hundai->brake";
        }
    }

public class Main3 {
    public static void main(String[] args) {
        Car car=new Car( 8,"Base Car" );
        System.out.println(car.starEngine());
        System.out.println(car.accelerate());
        System.out.println(car.brake());
        Swift swift=new Swift( 4,"Desire" );
        System.out.println(swift.starEngine());
        System.out.println(swift.accelerate());
        System.out.println(swift.brake());

       Hundai hundai=new Hundai( 8,"Creta" );

        System.out.println(hundai.starEngine());
        System.out.println(hundai.accelerate());
        System.out.println(hundai.brake());


    }

}