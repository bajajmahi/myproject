package src.main.java.practice;

public class VipPerson {
    public static void main(String[] args) {

        VipPerson person1 = new VipPerson();
        System.out.println( person1.getName() );
        VipPerson person2 = new VipPerson( "Bob", 5000.00 );
        System.out.println( person2.getName() );
        VipPerson person3 = new VipPerson( "MAhi", 4000.00, "bajajmahi123@gmail.com" );
        System.out.println( person3.getName() );
        System.out.println(person3.getCreditlLimits());
        System.out.println(person3.getEmailAddress());

    }
        private String name;
        private Double creditlLimits;
        private String emailAddress;

    public VipPerson() {
            this( "Default Name", 50000.00, "default@emial.com" );
        }
    public VipPerson( String name, Double creditlLimits ) {
            this( name, creditlLimits, "Unknown@email.com" );
        }

    public VipPerson( String name, Double creditlLimits, String emailAddress ) {
          this. name = name;
           this. creditlLimits = creditlLimits;
          this.  emailAddress = emailAddress;
        }

        public String getName () {
            return name;
        }

        public Double getCreditlLimits () {
            return creditlLimits;
        }

        public String getEmailAddress () {
            return emailAddress;
        }
    }
