package test.src.main.java.practice;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Locale;

public class DaysofWeek {

    public static void main(String args[]) {
        Calendar cal = Calendar.getInstance();
        DateFormatSymbols symbols = new DateFormatSymbols(new Locale("IND"));

        int day = cal.get(Calendar.DAY_OF_WEEK);
        System.out.print(day + "\n");
        String[] dayNames = symbols.getWeekdays();
        for (String s : dayNames) {
            System.out.print(s + "\n");
        }
        // System.out.println("Today is:"+cal.get(Calendar.));
    }
}
