package src.main.java.practice;

public class TotalCalculator {
    public static void main(String[] args) {

        Carpet carpet = new Carpet(3.5);
        Floor floor = new Floor(2.75, 4.0);
        TotalCalculator calculator = new TotalCalculator(floor, carpet);
        System.out.println("total= " + calculator.getTotalCost());
        carpet = new Carpet(1.5);
        floor = new Floor(5.4, 4.5);
        calculator = new TotalCalculator(floor, carpet);
        System.out.println("total= " + calculator.getTotalCost());

    }
    private Floor floor;
    private Carpet carpet;
    public TotalCalculator(Floor floor,Carpet carpet){
        this.floor=floor;
        this.carpet=carpet;
    }
    public double getTotalCost(){
        return carpet.getCost() * floor.getArea();
    }
}
