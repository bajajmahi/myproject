package src.main.java.practice;

public class PerfectNumber {
    public static void main(String[] args) {
        System.out.println(isPerfectNumber( 6));
    }
    public static boolean isPerfectNumber(int number) {
        if (number < 1) {
            return false;
        }
        int sum = 0;
        for (int i = 1; i < number; i++)
            if (number % i == 0) {
                sum = sum + i;
            }

            if (sum == number) {
            return true;
        } else {
            return false;
        }

    }
}

//A perfect number is a positive integer which is equal to the sum of its proper positive divisors.
//Proper positive divisors are positive integers that fully divide the perfect number without leaving a remainder and exclude the perfect number itself.