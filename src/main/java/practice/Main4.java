package src.main.java.practice;

public class Main4 {
    public static void main(String[] args) {
        Hamburger hamburger=new Hamburger( "wheat","chicken",true,true,false,false );
         hamburger.finalPrice(200 );


    }
}
class Hamburger {
    private String breadRollType;
    private String meat;
    private boolean lattuce;
    private boolean cheese;
    private boolean tomato;
    private boolean carrot;

    public Hamburger(String breadRollType, String meat, boolean lattuce, boolean cheese, boolean tomato, boolean carrot) {
        this.breadRollType = breadRollType;
        this.meat = meat;
        this.lattuce = lattuce;
        this.cheese = cheese;
        this.tomato = tomato;
        this.carrot = carrot;
    }

    public String getBreadRollType() {
        return breadRollType;
    }

    public String getMeat() {
        return meat;
    }

    public boolean isLattuce() {
        return lattuce;
    }

    public boolean isCheese() {
        return cheese;
    }

    public boolean isTomato() {
        return tomato;
    }

    public boolean isCarrot() {
        return carrot;
    }

    public int lattuce() {
        return 2;
    }

    public int cheese() {
        return 3;
    }

    public int tomato() {
        return 2;

    }

    public int carrot() {
        return 2;
    }

    public void finalPrice(int price) {

        int finalamount = 0;
        switch (price) {
            case 1:

                if (lattuce = true) {
                    finalamount = price + lattuce();
                    System.out.println( "you have to pay " + finalamount );
                }
                break;
            case 2:
                if (cheese = true) {
                    finalamount = price + cheese();
                    System.out.println( "you have to pay " + finalamount );
                }
                break;
            case 3:
                if (tomato = true) {
                    finalamount = price + tomato();
                    System.out.println( "you have to pay " + finalamount );
                }
                break;

        }

    }
}

class HealthyBurger extends Hamburger{
    private boolean brockly;
    private boolean chicken;


    public HealthyBurger(String breadRollType, String meat, boolean lattuce, boolean cheese, boolean tomato, boolean carrot, boolean brockly, boolean chicken) {
        super( "brown ray bread", meat, lattuce, cheese, tomato, carrot );
        this.brockly = brockly;
        this.chicken = chicken;

    }
    public boolean isBrockly() {
        return brockly;
    }

    public boolean isChicken() {
        return chicken;
    }


}
// The purpose of the application is to help a fictitious company called Bills Burgers to manage
// their process of selling hamburgers.
// Our application will help Bill to select types of burgers, some of the additional items (additions) to
// be added to the burgers and pricing.
// We want to create a base hamburger, but also two other types of hamburgers that are popular ones in Bills store.
// The basic hamburger should have the following items.
// Bread roll type, meat and up to 4 additional additions (things like lettuce, tomato, carrot, etc) that
// the customer can select to be added to the burger.
// Each one of these items gets charged an additional price so you need some way to track how many items got added
// and to calculate the final price (base burger with all the additions).
// This burger has a base price and the additions are all separately priced (up to 4 additions, see above).
// Create a Hamburger class to deal with all the above.
// The constructor should only include the roll type, meat and price, can also include name of burger or you
// can use a setter.
// Also create two extra varieties of Hamburgers (subclasses) to cater for
// a) Healthy burger (on a brown rye bread roll), plus two addition items that can be added.
// The healthy burger can have 6 items (Additions) in total.
// hint:  you probably want to process the two additional items in this new class (subclass of Hamburger),
// not the base class (Hamburger), since the two additions are only appropriate for this new class
// (in other words new burger type).
// b) Deluxe hamburger - comes with chips and drinks as additions, but no extra additions are allowed.
// hint:  You have to find a way to automatically add these new additions at the time the deluxe burger
// object is created, and then prevent other additions being made.
//  All 3 classes should have a method that can be called anytime to show the base price of the hamburger
// plus all additionals, each showing the addition name, and addition price, and a grand/final total for the
// burger (base price + all additions)
// For the two additional classes this may require you to be looking at the base class for pricing and then
// adding totals to final price.