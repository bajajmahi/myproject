package src.main.java.practice;
import java.util.*;

public class InputCalculator {
    public static void main(String[] args) {

        Scanner scanner=new Scanner(System.in);
        int sum=0;
        long average;
        int count=0;
        while(true) {
            boolean isAnInt = scanner.hasNextInt();
            if (isAnInt) {
                int number = scanner.nextInt();
                sum += number;
            } else {
                break;
            }
            count++;
        }
            if (sum != 0) {
                average = (long) Math.round((double)sum /count);
            }else{
                average=0;
            }

            System.out.println("SUM = " + sum +
                    " AVG = " + average);
scanner.close();
        }


    }


