package src.main.java.practice;
import java.util.*;

public class Demo3 {
    public static void main(String[] args) {


        int sum = 0;
        long avg;
        int count = 0;

        Scanner scanner = new Scanner( System.in );

        while (true) {
            boolean isAnInt = scanner.hasNextInt();

            if (isAnInt) {
                int number = scanner.nextInt();
                sum += number;
            } else {
                break;
            }
            count++;
        }
        if (sum != 0) {
            avg = (long) Math.round( (double) sum / count );
        } else {
            avg = 0;
        }
        System.out.println( "SUM = " + sum + " AVG = " + avg );
        scanner.close();
    }

}