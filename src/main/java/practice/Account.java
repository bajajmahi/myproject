package src.main.java.practice;

public class Account {
    public static void main(String[] args) {
        Account bobsaccount=new Account();
        bobsaccount.setAccountNumber("123456");
        bobsaccount.setCustomerName( "Mahi" );
        bobsaccount.withDrawal( 100 );
        bobsaccount.deposite( 50 );
    }
    private String accountNumber;
    private double balance;
    private String customerName;
    private String customerEmailAddress;
    private String customerPhoneNumber;
    public void deposite(double depositeAmount){
        this.balance+=depositeAmount;
    }
    public void withDrawal(double withdrawalAmount){
        if(this.balance-withdrawalAmount<0){
            System.out.println("Only" +balance+ "available.withdrawal can not be processed");
        }else{
            this.balance-=withdrawalAmount;
            System.out.println("withdrawal of" +withdrawalAmount+ "processed. Remaining Balance is" +balance);
        }
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmailAddress() {
        return customerEmailAddress;
    }

    public void setCustomerEmailAddress(String customerEmailAddress) {
        this.customerEmailAddress = customerEmailAddress;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }
}
