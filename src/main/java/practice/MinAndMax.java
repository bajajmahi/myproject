package src.main.java.practice;

import java.util.Scanner;

public class MinAndMax {
    public static void main(String[] args) {

        Scanner scanner = new Scanner( System.in );

        int min = 0;
        int max = 0;
        boolean first=true;
        while (true) {
            System.out.println( "Enter Number" );
            boolean IsAnInt = scanner.hasNextInt();
            if (IsAnInt) {
                int number = scanner.nextInt();
                if(first){
                    first=false;
                    min=number;
                    max=number;
                }
                if (number > max) {

                    max = number;
                }
                if (number < min) {
                    min = number;
                }

            } else {
                break;
            }
            scanner.nextLine();

        }
        System.out.println( "Min =  " + min + " Max = 1" + max );

        scanner.close();

    }
}
