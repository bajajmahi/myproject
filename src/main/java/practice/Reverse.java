package src.main.java.practice;

import src.main.java.practice.Array.Array;

import java.util.Arrays;

public class Reverse {
    public static void main(String[] args) {
        int []array={1,4,6,8,9};
        System.out.println("Array = "+ Arrays.toString(array) );
        reverseNumber( array );
        System.out.println("Reversed Array =" + Arrays.toString( array));

    }
    private static void reverseNumber(int [] array){
        int maxindex=array.length-1;
        int halfindex=array.length/2;
        for(int i=0;i<halfindex;i++){
            int temp=array[i];
            array[i]=array[maxindex-i];
            array[maxindex-i]=temp;
        }
    }
}


