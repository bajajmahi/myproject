package test.src.main.java.course;

public class primenumber {

    public static void main(String[] args) {
        int count = 0;
        for (int i = 10; i <= 100; i++) {
            if (primeNumber(i)) {
                count++;
                System.out.println("number " + i + " is prime number");
                if (count == 100) {
                    System.out.println("exit for loop");
                    break;
                }
            }

        }
    }

    public static boolean primeNumber(int n) {

        if (n == 1) {
            return false;
        }
        for (int i = 2; i <= n / 2; i++) {
            if (n % 2 == 0) {
                return false;
            }
        }
        return true;
    }
}
