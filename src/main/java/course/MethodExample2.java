package test.src.main.java.course;

public class MethodExample2 {

    public static String MinutesAndSecond(long minutes, long seconds) {
        if ((minutes < 0) || (seconds < 0) && (seconds > 59)) {
            System.out.println("invalid Values");
            return "invalid Values";
        }
        long hours = minutes / 60;
        long remainingminutes = minutes % 60;
        return hours + "h " + minutes + "m " + seconds + "s ";

    }

    public static String MinutesAndSecond(long seconds) {
        if ((seconds < 0)) {
            return "invalid values";
        }
        long minutes = seconds / 60;
        long remainingseconds = seconds % 60;
        return (MinutesAndSecond(minutes, remainingseconds));
    }

    public static void main(String[] args) {

        System.out.println(MinutesAndSecond(65, 45));
        System.out.println(MinutesAndSecond(3945L));
    }

}
