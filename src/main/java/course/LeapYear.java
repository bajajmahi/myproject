package test.src.main.java.course;

public class LeapYear {

    public static boolean leapyear(int year) {
        if (year >= 1 && year <= 9999) {

            if ((year % 400 == 0 || year % 100 != 0) && (year % 4 == 0)) {

                System.out.println("This year is leap year");
                return true;

            } else
                System.out.println("this is not leap year");
            return false;
        }
        return false;
    }

    public static void main(String[] args) {
        leapyear(1998);


    }
}

