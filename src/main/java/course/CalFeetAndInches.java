package test.src.main.java.course;

public class CalFeetAndInches {

    public static double CalFeetAndInchesInCentimeter(double feet, double inches) {

        if ((feet < 0) || (inches < 0) && (inches >= 12)) {
            System.out.println("invalid values");
            return -1;
        }
        double Centimeters = (feet * 12) * 2.54;
        Centimeters += inches * 2.54;
        System.out.println(feet + " FEET and " + inches + " inches = " + Centimeters + " cm ");
        return Centimeters;
    }

    public static double CalFeetAndInchesInCentimeter(double inches) {

        if ((inches >= 0)) {
            double feet = inches / 12;
            System.out.println(+inches + " Inch = " + feet + "Ft");
            return feet;
        } else {
            System.out.println("invalid values");
            return -1;
        }
    }


    public static void main(String args[]) {

        CalFeetAndInchesInCentimeter(3, 5);
        CalFeetAndInchesInCentimeter(9);

    }
}

