package test.src.main.java.course;

public class switchjava {

    public static void printDayofweek(int day) {
        // day=4;
        switch (day) {
            case 0:
                System.out.println("It is sunday");
                break;
            case 1:
                System.out.println("It is Monday");
                break;
            case 2:
                System.out.println("It is Tuesday");
                break;
            case 3:
                System.out.println("It is Wednesday");
                break;
            case 4:
                System.out.println("It is Thursday");
                break;
            case 5:
                System.out.println("It is friday");
                break;
            case 6:
                System.out.println("It is Saturday");
                break;
            default:
                System.out.println("invalid Value");
                break;
        }

    }

    public static void main(String[] args) {
        printDayofweek(4);

    }
}
